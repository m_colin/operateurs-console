const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question('Enter rank of Fibonacci number : ', (n) => {
    let fibArray = [0, 1];
    if (isNaN(n) == false) {
        for (let index = 2; fibArray.length < n; index++) {
            fibArray.push(fibArray[index-1]+fibArray[index-2]);
        }
        console.log('Fibonacci suite: ', fibArray);
        console.log('Fibonacci number at rank ', n, ' is ', fibArray[fibArray.length - 1]);
    }
})
