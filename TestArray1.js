const array = [1, 15, -3, 0, 8, 7, 4, -2, 28, 7, -1, 17, 2, 3, 0, 14, -4];

// 1
function all() {
    console.log('Element du tableau');
    array.forEach(element => {
        console.log(element);
    });
}

// 2
function reverse() {
    console.log('Ordre inverse');
    array.reverse().forEach(element => {
        console.log(element);
    });
}


// 3
function sup3() {
    console.log('Supérieur à 3');
    array.forEach(element => {
        if (element > 3) {
            console.log(element);
        }
    });
}


// 4
function even() {
    console.log('Nombre pair');
    array.forEach(element => {
        if (element % 2 == 0) {

            console.log(element);
        }
    });
}

function highest() {
    let temp = 0;
    for (var i = 0; i < array.length; i++) {
        if(array[i] > temp){
            temp = array[i]
        }
    }
    console.log('Plus grand');
    console.log(temp);
}

function lowest() {
    let temp = 0;
    for (var i = 0; i < array.length; i++) {
        if(array[i] < temp){
            temp = array[i]
        }
    }
    console.log('Plus petit');
    console.log(temp);
}

all();
reverse();
sup3();
even();
highest();
lowest();
